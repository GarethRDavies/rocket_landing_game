﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    [SerializeField] private float _rcsThrust = 100f;
    [SerializeField] private float _mainThrust = 100f;
    [SerializeField] private float _levelLoadDelay = 2f;
    [SerializeField] private AudioClip _mainEngine;
    [SerializeField] private AudioClip _deathSound;
    [SerializeField] private AudioClip _levelLoadSound;

    [SerializeField] private ParticleSystem _mainEngineParticles;
    [SerializeField] private ParticleSystem _deathParticles;
    [SerializeField] private ParticleSystem _successParticles;

    private Rigidbody _myRigidbody;
    private AudioSource _audioSource;

    private bool _isTransitioning = false;
    private bool _collisionsDisabled = false;

    //[SerializeField] private float _velocity;

    // Start is called before the first frame update
    void Start()
    {
        _myRigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isTransitioning)
        {
            RespondToThrustInput();
            RespondToRotateInput();
        }
        //todo only if debug on
        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            //toggle collision
            _collisionsDisabled = !_collisionsDisabled; //toggle
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_isTransitioning || _collisionsDisabled)
        {
            return;
        }

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                //do nothing
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartSuccessSequence()
    {
        _isTransitioning = true;
        _audioSource.Stop();
        _audioSource.PlayOneShot(_levelLoadSound);
        _successParticles.Play();
        Invoke("LoadNextLevel", _levelLoadDelay);
    }

    private void StartDeathSequence()
    {
        _isTransitioning = true;
        _audioSource.Stop();
        _audioSource.PlayOneShot(_deathSound);
        _deathParticles.Play();
        Invoke("LoadFirstLevel", _levelLoadDelay);
    }

    private void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

    private void LoadFirstLevel()
    {
        SceneManager.LoadScene(0);
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();
        }
        else
        {
            StopApplyingThrust();
        }
    }

    private void StopApplyingThrust()
    {
        _audioSource.Stop();
        _mainEngineParticles.Stop();
    }

    private void ApplyThrust()
    {
        //thrust
        float thrustThisFrame = _mainThrust * Time.deltaTime;
        _myRigidbody.AddRelativeForce(Vector3.up * thrustThisFrame);
        if (!_audioSource.isPlaying)
        {
            _audioSource.PlayOneShot(_mainEngine);
        }
        _mainEngineParticles.Play();
    }

    private void RespondToRotateInput()
    {
        float rotationThisFrame = _rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            //rotate left
            RotateManually(rotationThisFrame);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //rotate right
            RotateManually(-rotationThisFrame);
        }
    }

    private void RotateManually(float rotationThisFrame)
    {
        _myRigidbody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame);
        _myRigidbody.freezeRotation = false;
    }
}
